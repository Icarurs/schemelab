# Scheme Lab

CSE 3341 Lab 4. The assignment is to write a function, `eliminateNsort`, which will receive two lists of numbers as arguments. Suppose L1, L2 are the two lists. The result returned by the function should be obtained from L1, L2 as follows: start with L1 and remove from it all those elements that are also in L2; then sort the remaining elements into non-decreasing order to get the result list.
