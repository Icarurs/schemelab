; Checks whether L contains element.
; First check if L is null. If it is, return false.
; Check if (car L) is the same as element, and return the result.
; Otherwise, recursively call contains? on (cdr L).
(define (contains? L element)
  (cond ((null? L) #f)
        ((eqv? (car L) element))
        (#t (contains? (cdr L) element)) ))

; Remove all instances of element e from list L.
; First check if L is null. If it is, return L.
; Check if (car L) matches e. Then, recursively call removeElt on (cdr L).
; Otherwise, call removeElt on (cdr L) and append it to (car L).
(define (removeElt L e)
  (cond ((null? L) L)
        ((eqv? (car L) e) (removeElt (cdr L) e))
        (#t (cons (car L) (removeElt (cdr L) e))) ))

; Returns new list that has all elements of L1 except those in L2.
; Return L1 if L2 is null.
; If the first element in L2 is contained in L1, then remove that element from
; L1, then recursively call setSubtract on L1 and (cdr L2).
(define (setSubtract L1 L2)
  (cond ((null? L2) L1)
        ((contains? L1 (car L2))
         (setSubtract (removeElt L1 (car L2)) (cdr L2)))
        (#t (setSubtract L1 (cdr L2))) ))

; Merges two lists with elements in order.
; Return L2 if L1 is null and vice versa.
; Otherwise, if (car L1) < (car L2), then append the result of recursively
; calling merge on (cdr L1) and L2 to (car L1) and vice versa.
(define (merge L1 L2)
  (cond ((null? L1) L2)
        ((null? L2) L1)
        ((<= (car L1) (car L2)) (cons (car L1) (merge (cdr L1) L2)))
        (#t (cons (car L2) (merge L1 (cdr L2)))) ))

; Sorts a list with elements in order.
; If L is null, return L.
; If (cdr L) is null, return L.
; Otherwise merge the first two elements of L. Call sort recursively on
; (cddr L) and merge the two results together.
(define (sort L)
  (cond ((null? L) L)
        ((null? (cdr L)) L)
        (#t (merge (merge (cons (car L) `()) (cons (cadr L) `()))
                   (sort (cddr L)))) ))

; Remove elements of L2 from L1, then sort remaining elements in L1.
(define (eliminateNsort L1 L2) (sort (setSubtract L1 L2)))
